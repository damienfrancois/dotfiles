set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'mbbill/undotree'
Plugin 'tpope/vim-fugitive'
Plugin 'terryma/vim-expand-region'
Plugin 'vim-scripts/matchit.zip'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'justinmk/vim-sneak'
Plugin 'tpope/vim-speeddating'
Plugin 'tpope/vim-surround'
Plugin 'godlygeek/tabular'
Plugin 'drmikehenry/vim-extline'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'
Plugin 'vim-scripts/VOoM'
Plugin 'tomtom/tcomment_vim'
Plugin 'tpope/vim-endwise'
Plugin 'mattn/emmet-vim'
Plugin 'altercation/vim-colors-solarized.git'
Plugin 'tmhedberg/SimpylFold'
Plugin 'tpope/vim-repeat'
Plugin 'papanikge/vim-voogle'
Plugin 'junegunn/vim-easy-align'
Plugin 'freeo/vim-kalisi'
Plugin 'plasticboy/vim-markdown'
Plugin 'reedes/vim-pencil'
Plugin 'reedes/vim-litecorrect'
Plugin 'saltstack/salt-vim'
Plugin 'Glench/Vim-Jinja2-Syntax'
Plugin 'damienfrancois/ansible-vim'
Plugin 'MicahElliott/Rocannon'
Plugin 'bling/vim-airline'
Plugin 'vim-scripts/vcscommand.vim'
Plugin 'nblock/vim-dokuwiki'
Plugin 'scrooloose/syntastic'
Plugin 'Rykka/InstantRst'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
Plugin 'SearchComplete'
Plugin 'DrawIt'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
"Plugin 'user/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


filetype plugin indent on " enable loading the indent file for specific file types and plugins

let mapleader =','

set backspace=2
set expandtab
set foldlevel=99
set encoding=utf-8
set foldmethod=indent
set hls
set incsearch
set shiftwidth=4
set smarttab
set splitright
set tabstop=4
set wrap linebreak nolist
set lazyredraw  " redraw only when we need to.
" case insensitive search
set ignorecase
set smartcase
" move vertically by visual line
nnoremap j gj
nnoremap k gk

" highlight last inserted text
nnoremap gV `[v`]

" jk is escape
inoremap jk <esc>

set undofile                " Save undo's after file closes
set undodir=$HOME/.vim/undo " where to save undo histories
set undolevels=1000         " How many undos
set undoreload=10000        " number of lines to save for undo

set t_ti= t_te=  " prevent vim from clearing screen on exit

syntax on

" CursorLine
set cursorline
highlight  CursorLine ctermbg=None ctermfg=None cterm=None
autocmd InsertEnter * highlight  CursorLine ctermbg=darkgrey ctermfg=None cterm=None
autocmd InsertLeave * highlight  CursorLine ctermbg=None     ctermfg=None cterm=None

" Set a specific colorscheme for vimdiff
" Fix the difficult-to-read default setting for diff text highlighting.  The
" bang (!) is required since we are overwriting the DiffText setting.
highlight! link DiffText Todo
highlight! DiffChange cterm=bold ctermbg=DarkGrey gui=none guifg=bg guibg=Red
highlight! DiffDelete cterm=bold ctermbg=DarkGrey gui=none guifg=bg guibg=Red
highlight! DiffAdd cterm=bold ctermbg=DarkGrey gui=none guifg=bg guibg=Red
highlight! Folded cterm=bold ctermbg=DarkGrey

" Set custom spell check highlight color
hi SpellBad    ctermfg=015      ctermbg=000     cterm=none      guifg=#FFFFFF   guibg=#000000   gui=none

" Macvim color scheme
let macvim_skip_colorscheme = 1
"if has("gui_macvim")
"    set transparency=15
"endif

augroup litecorrect
  autocmd!
  autocmd FileType mkd.markdown call litecorrect#init()
  autocmd FileType textile call litecorrect#init()
augroup END

nnoremap <F5> :UndotreeToggle<cr>

" Allow . to work in visual mode on all the selection
vnoremap . :norm.<CR>

" Documentation for ansible
let g:ansible_options = {'documentation_mapping': '<C-K>'}

"Show command in bottom right portion of the screen
set showcmd
" More useful command-line completion
set wildmenu
"Auto-completion menu
set wildmode=list:longest
"Bubble multiple lines
vmap <C-k> xkP`[V`]
vmap <C-j> xp`[V`]

" Saves file when Vim window loses focus
au FocusLost * :wa
let g:rocannon_bypass_colorscheme = 1

" To enable vim-airline
set laststatus=2
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
      let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
let g:airline#extensions#branch#use_vcscommand = 1

set guifont=Hack\ Regular:h11


set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

