alias ls='ls -G'
alias ll='ls -lX -G --color  -h  --group-directories-first --hide="Icon?"'
alias port='sudo port'
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

export PATH=/opt/local/bin:/opt/local/sbin:$PATH
export PATH=/usr/local/bin:/usr/local/sbin:$PATH

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

export HISTTIMEFORMAT="%F %T "

export PS1='\[\033[1;33m\]\u\[\033[1;37m\]@\[\033[1;32m\]\h\[\033[1;37m\]:\[\033[1;31m\]\w\[\033[1;36m\] \$ \[\033[0m\]'
export PROMPT_COMMAND='echo -ne "\033]0;${USER}@$(hostname -s)\007"'

[ -f ~/.vagrant_completion.sh ] && source ~/.vagrant_completion.sh 

if [[ $- == *i*  ]] ; then
if which tmux &> /dev/null  && tmux list-sessions &>/dev/null ; then
echo tmux sessions:
tmux list-sessions
fi
fi
#TODO: Do the same with screen

if [[ $- == *i* ]] ; then
shopt -s autocd
test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"
fi

[ -f ~/.bash_profile.local ] && source ~/.bash_profile.local

man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[1;31m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;32m") \
            man "$@"
}

export HOMEBREW_GITHUB_API_TOKEN=9afd7f472b673483aefe38846045e16b5d244547
